<?php 


function cf_plugin_metaboxes() {
	
	$screens = array( 'page' );
	
	$metaboxes = array (
		array (
			'ID'			=> 'cf_gallery_metaboxes',
			'title'			=> __( 'Gallery', 'crystal-fairy-plugin' ),
			'callback'		=> 'meta_box_callback',
			'screens'		=> array( 'page' ),
			'template'		=> 'page-gallery.php',
			'context'		=> 'normal',
			'priority'		=> 'default',
			'fields'		=> array (
				array (
					'ID'		=> 'cf_page_vertical_image',
					'name'		=> 'cf_page_vertical_image',
					'title'		=> __( 'Vertical Image', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> '',
					'description'	=> __( 'This is the tall image that appears next to the content', 'crystal-fairy-plugin' )
				),
				array (
					'ID'		=> 'cf_page_gallery',
					'name'		=> 'cf_page_gallery',
					'title'		=> __( 'Gallery', 'crystal-fairy-plugin' ),
					'type'		=> 'wysiwyg',
					'teeny'		=> true,
					'class'		=> '',
					'description'	=> __( 'Use this field to add a gallery below the content', 'crystal-fairy-plugin' )
				),
			),
		),
		array (
			'ID'			=> 'cf_gallery_metaboxes',
			'title'			=> __( 'Gallery', 'crystal-fairy-plugin' ),
			'callback'		=> 'meta_box_callback',
			'screens'		=> array( 'page' ),
			'template'		=> 'page-gallery.php',
			'context'		=> 'normal',
			'priority'		=> 'default',
			'fields'		=> array (
				array (
					'ID'		=> 'cf_page_vertical_image',
					'name'		=> 'cf_page_vertical_image',
					'title'		=> __( 'Vertical Image', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> '',
					'description'	=> __( 'This is the tall image that appears next to the content', 'crystal-fairy-plugin' )
				),
				array (
					'ID'		=> 'cf_page_gallery',
					'name'		=> 'cf_page_gallery',
					'title'		=> __( 'Gallery', 'crystal-fairy-plugin' ),
					'type'		=> 'wysiwyg',
					'teeny'		=> true,
					'class'		=> '',
					'description'	=> __( 'Use this field to add a gallery below the content', 'crystal-fairy-plugin' )
				),
			),
		),
		array (
			'ID'			=> 'cf_experience_metaboxes',
			'title'			=> __( 'Experience', 'crystal-fairy-plugin' ),
			'callback'		=> 'meta_box_callback',
			'screens'		=> array( 'page' ),
			'template'		=> 'page-experience.php',
			'context'		=> 'normal',
			'priority'		=> 'default',
			'fields'		=> array (
				array (
					'ID'		=> 'cf_experience_heading',
					'name'		=> 'cf_experience_heading',
					'title'		=> __( 'Find Out More Heading', 'crystal-fairy-plugin' ),
					'type'		=> 'text',
					'class'		=> '',
				),
				array (
					'ID'		=> 'cf_our_service_image',
					'name'		=> 'cf_page_vertical_image',
					'title'		=> __( 'Our Service Image', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> '',
					'description'	=> __( 'The image used to represent and link to the Our Service page', 'crystal-fairy-plugin' )
				),
				array (
					'ID'		=> 'cf_our_boutique_image',
					'name'		=> 'cf_our_boutique_image',
					'title'		=> __( 'Our Boutique Image', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> '',
					'description'	=> __( 'The image used to represent and link to the Our Boutique page', 'crystal-fairy-plugin' )
				),
				array (
					'ID'		=> 'cf_testimonials_image',
					'name'		=> 'cf_testimonials_image',
					'title'		=> __( 'Testimonials Image', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> '',
					'description'	=> __( 'The image used to represent and link to the Testimonials page', 'crystal-fairy-plugin' )
				),
			),
			
		),
		array (
			'ID'			=> 'cf_accessories_metaboxes',
			'title'			=> __( 'Accessories', 'crystal-fairy-plugin' ),
			'callback'		=> 'meta_box_callback',
			'screens'		=> array( 'page' ),
			'template'		=> 'page-accessories.php',
			'context'		=> 'normal',
			'priority'		=> 'default',
			'fields'		=> array (
				array (
					'ID'		=> 'cf_accessories_title_1',
					'name'		=> 'cf_accessories_title_1',
					'title'		=> __( 'Title 1', 'crystal-fairy-plugin' ),
					'type'		=> 'text',
					'class'		=> 'wide',
				),
				array (
					'ID'		=> 'cf_accessories_text_1',
					'name'		=> 'cf_accessories_text_1',
					'title'		=> __( 'Text 1', 'crystal-fairy-plugin' ),
					'type'		=> 'wysiwyg',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_image_1',
					'name'		=> 'cf_accessories_image_1',
					'title'		=> __( 'Image 1', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_divider_1',
					'name'		=> 'cf_accessories_divider_1',
					'type'		=> 'divider',
					'class'		=> '',
				),
				array (
					'ID'		=> 'cf_accessories_title_2',
					'name'		=> 'cf_accessories_title_2',
					'title'		=> __( 'Title 2', 'crystal-fairy-plugin' ),
					'type'		=> 'text',
					'class'		=> 'wide',
				),
				array (
					'ID'		=> 'cf_accessories_text_2',
					'name'		=> 'cf_accessories_text_2',
					'title'		=> __( 'Text 2', 'crystal-fairy-plugin' ),
					'type'		=> 'wysiwyg',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_image_2',
					'name'		=> 'cf_accessories_image_2',
					'title'		=> __( 'Image 2', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_divider_2',
					'name'		=> 'cf_accessories_divider_2',
					'type'		=> 'divider',
					'class'		=> '',
				),
				array (
					'ID'		=> 'cf_accessories_title_3',
					'name'		=> 'cf_accessories_title_3',
					'title'		=> __( 'Title 3', 'crystal-fairy-plugin' ),
					'type'		=> 'text',
					'class'		=> 'wide',
				),
				array (
					'ID'		=> 'cf_accessories_text_3',
					'name'		=> 'cf_accessories_text_3',
					'title'		=> __( 'Text 3', 'crystal-fairy-plugin' ),
					'type'		=> 'wysiwyg',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_image_3',
					'name'		=> 'cf_accessories_image_3',
					'title'		=> __( 'Image 3', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_divider_3',
					'name'		=> 'cf_accessories_divider_3',
					'type'		=> 'divider',
					'class'		=> '',
				),
				
				array (
					'ID'		=> 'cf_accessories_title_4',
					'name'		=> 'cf_accessories_title_4',
					'title'		=> __( 'Title 4', 'crystal-fairy-plugin' ),
					'type'		=> 'text',
					'class'		=> 'wide',
				),
				array (
					'ID'		=> 'cf_accessories_text_4',
					'name'		=> 'cf_accessories_text_4',
					'title'		=> __( 'Text 4', 'crystal-fairy-plugin' ),
					'type'		=> 'wysiwyg',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_image_4',
					'name'		=> 'cf_accessories_image_4',
					'title'		=> __( 'Image 4', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_divider_4',
					'name'		=> 'cf_accessories_divider_4',
					'type'		=> 'divider',
					'class'		=> '',
				),
				
				array (
					'ID'		=> 'cf_accessories_title_5',
					'name'		=> 'cf_accessories_title_5',
					'title'		=> __( 'Title 5', 'crystal-fairy-plugin' ),
					'type'		=> 'text',
					'class'		=> 'wide',
				),
				array (
					'ID'		=> 'cf_accessories_text_5',
					'name'		=> 'cf_accessories_text_5',
					'title'		=> __( 'Text 5', 'crystal-fairy-plugin' ),
					'type'		=> 'wysiwyg',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_image_5',
					'name'		=> 'cf_accessories_image_5',
					'title'		=> __( 'Image 5', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_divider_5',
					'name'		=> 'cf_accessories_divider_5',
					'type'		=> 'divider',
					'class'		=> '',
				),
				array (
					'ID'		=> 'cf_accessories_title_6',
					'name'		=> 'cf_accessories_title_6',
					'title'		=> __( 'Title 6', 'crystal-fairy-plugin' ),
					'type'		=> 'text',
					'class'		=> 'wide',
				),
				array (
					'ID'		=> 'cf_accessories_text_6',
					'name'		=> 'cf_accessories_text_6',
					'title'		=> __( 'Text 6', 'crystal-fairy-plugin' ),
					'type'		=> 'wysiwyg',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_image_6',
					'name'		=> 'cf_accessories_image_6',
					'title'		=> __( 'Image 6', 'crystal-fairy-plugin' ),
					'type'		=> 'image',
					'class'		=> 'cf-metafield-half',
				),
				array (
					'ID'		=> 'cf_accessories_divider_6',
					'name'		=> 'cf_accessories_divider_6',
					'type'		=> 'divider',
					'class'		=> '',
				),
			),
		),
		array (
			'ID'			=> 'cf_collections_metaboxes',
			'title'			=> __( 'Collections', 'crystal-fairy-plugin' ),
			'callback'		=> 'meta_box_callback',
			'screens'		=> array( 'page' ),
			'template'		=> 'page-collection.php',
			'context'		=> 'side',
			'priority'		=> 'default',
			'fields'		=> array (
				array (
					'ID'		=> 'cf_collections_columns',
					'name'		=> 'cf_collections_columns',
					'title'		=> __( 'Columns', 'crystal-fairy-plugin' ),
					'type'		=> 'select',
					'options'	=> array(
						'2'		=> '2',
						'3'		=> '3',
						'4'		=> '4'
					),
					'class'		=> '',
				),
			),
		),
		array (
			'ID'			=> 'cf_testimonials_metaboxes',
			'title'			=> __( 'Testimonial Meta', 'crystal-fairy-plugin' ),
			'callback'		=> 'meta_box_callback',
			'screens'		=> array( 'testimonials' ),
			'context'		=> 'normal',
			'priority'		=> 'default',
			'fields'		=> array (
				array (
					'ID'		=> 'cf_testimonial_date',
					'name'		=> 'cf_testimonial_date',
					'title'		=> __( 'Date', 'crystal-fairy-plugin' ),
					'type'		=> 'text',
					'class'		=> '',
					'description'	=> ''
				),
				array (
					'ID'		=> 'cf_featured_testimonial',
					'name'		=> 'cf_featured_testimonial',
					'title'		=> __( 'Featured', 'crystal-fairy-plugin' ),
					'type'		=> 'checkbox',
					'class'		=> '',
					'description'	=> ''
				),
			),
			
		),
		
	);

	return $metaboxes;
	
}