<?php
/*
 * CF_Metaboxes
*/

// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Plugin public class
 **/
if( ! class_exists( 'CF_Metaboxes' ) ) {

	class CF_Metaboxes {
	
		public $metaboxes;

		public function __construct( $metaboxes ) {
			$this -> metaboxes = $metaboxes;
		}
		
		/*
		 * Initialize the class and start calling our hooks and filters
		 * @since 1.0.0
		 */
		public function init() {
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
			add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
			add_action( 'save_post', array( $this, 'save_metabox_data' ) );
		}
		
		/*
		 * Register the metabox
		 * @since 1.0.0
		 */
		public function add_meta_box() {
			
			global $post;
			$screens = array( 'post', 'page' );
			$metaboxes = $this -> metaboxes;
			
			foreach( $metaboxes as $metabox ) {
				
				$add_box = true;
				
				// If a template is specified, check it matches current page template
				if( isset( $metabox['template'] ) ) {
					$page_template = get_post_meta( $post->ID, '_wp_page_template', true );
					if( $metabox['template'] != $page_template ) {
						$add_box = false;
					}
				}

				if( $add_box ) {
					add_meta_box (
						$metabox['ID'],
						$metabox['title'],
						array( $this, $metabox['callback'] ),
						$metabox['screens'],
						$metabox['context'],
						$metabox['priority'],
						$metabox['fields']
					);
				}
				
				
			}
			
		}
		
		/*
		 * Metabox callbacks
		 * @since 1.0.0
		*/
		public function meta_box_callback( $post, $fields ) {

			wp_nonce_field( 'save_metabox_data', 'cf_metabox_nonce' );
			
			if( $fields['args'] ) {
				
				foreach( $fields['args'] as $field ) {
						
					switch( $field['type'] ) {
						
						case 'image':
							$this -> metabox_image_output( $post, $field );
							break;
						case 'checkbox':
							$this -> metabox_checkbox_output( $post, $field );
							break;
						case 'text':
							$this -> metabox_text_output( $post, $field );
							break;
						case 'select':
							$this -> metabox_select_output( $post, $field );
							break;
						case 'wysiwyg':
							$this -> metabox_wysiwyg_output( $post, $field );
							break;
						case 'divider':
							$this -> metabox_divider_output( $post, $field );
							break;
					}
						
				}
				
			}

		}
		
		/*
		 * Metabox callback for image type
		 * @since 1.0.0
		 */
		public function metabox_image_output( $post, $field ) {
			
			$value = get_post_meta( $post -> ID, $field['ID'], true );
			
			?>
			<div id="item-<?php echo $field['name']; ?>" class="cf-metafield <?php echo $field['class']; ?>">
				
				<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
				<input type="hidden" class="cf-image-id" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo esc_attr( $value ); ?>" >
				
				<div class="gallery-thumb">
					<?php if( isset( $value ) ) { ?>
						<?php echo wp_get_attachment_image( intval( $value ), 'medium' ); ?>
					<?php } ?>
				</div>
				
				<input type="button" class="button cf-work-media-upload" data-metabox="<?php echo $field['name']; ?>" value="<?php _e( 'Add Image', 'crystal-fairy-plugin' )?>" />
				
				<input type="button" class="button cf-work-media-remove" data-metabox="<?php echo $field['name']; ?>" value="<?php _e( 'Remove Image', 'crystal-fairy-plugin' )?>" />
				
				<?php if( isset( $field['description'] ) ) { ?>
					<p class="description"><?php echo esc_html( $field['description'] ); ?></p>
				<?php } ?>

			</div>
			<?php
		}
		
		/**
		 * Metabox callback for checkbox
		 * @since 1.0.0
		 */
		public function metabox_checkbox_output( $post, $field ) {
			
			$field_value = 0;

			// First check if we're on the post-new screen
			global $pagenow;
			if( in_array( $pagenow, array( 'post-new.php' ) ) ) {
				// This is a new post screen so we can apply the default value
				$field_value = $field['default'];
			} else {		
				$custom = get_post_custom( $post->ID );
				if( isset( $custom[$field['ID']][0] ) ) {
					$field_value = $custom[$field['ID']][0];
				}
			}
			?>
			<div class="cf-metafield <?php echo $field['class']; ?>">
				<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
				<input type="checkbox" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="1" <?php checked( 1, $field_value ); ?>>
				<?php if( ! empty( $field['label'] ) ) { ?>
					<?php echo $field['label']; ?>
				<?php } ?>
				<?php if( ! empty( $field['description'] ) ) { ?>
					<p class="description"><?php echo $field['description']; ?></p>
				<?php } ?>
			</div>
			<?php
		}
		
		/**
		 * Metabox callback for text
		 * @since 1.0.0
		 */
		public function metabox_text_output( $post, $field ) {
			
			$value = get_post_meta( $post -> ID, $field['ID'], true ); ?>
			<div class="cf-metafield <?php echo $field['class']; ?>">
				<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
				<input type="text" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo esc_attr( $value ); ?>">
				<?php if( ! empty( $field['label'] ) ) { ?>
					<?php echo $field['label']; ?>
				<?php } ?>
				<?php if( ! empty( $field['description'] ) ) { ?>
					<p class="description"><?php echo $field['description']; ?></p>
				<?php } ?>
			</div>
			<?php
		}
		
		/*
		 * Metabox callback for wysiwyg type
		 * @since 1.0.0
		 */
		public function metabox_wysiwyg_output( $post, $field ) {
			
			$value = get_post_meta( $post -> ID, $field['ID'], true );
			$teeny = false;
			if( isset( $field['teeny'] ) ) {
				$teeny = $field['teeny'];
			}
			?>
			<div class="cf-metafield <?php echo $field['class']; ?>">
				<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
				<?php wp_editor (
					htmlspecialchars_decode( $value ),
					$field['name'],
					array (
						"media_buttons" => false,
						"textarea_rows" => 15,
						"media_buttons" => true,
						"teeny"			=> $teeny
					)
				); ?>
			</div>
			<?php
		}
		
		
		/*
		 * Metabox callback for divider
		 * @since 1.0.0
		 */
		public function metabox_divider_output( $post, $field ) {
			?>
			<div class="cf-metafield <?php echo $field['class']; ?>">
				<hr>
			</div>
			<?php
		}
		
		
		/*
		 * Metabox callback for select
		 * @since 1.0.0
		 */
		public function metabox_select_output( $post, $field ) {
			
			$field_value = get_post_meta( $post -> ID, $field['ID'], true );
			
			// If there's no saved value and a default value exists, set the value to the default
			// This is to ensure certain settings are set automatically
			if( empty( $field_value ) && ! empty( $field['default'] ) ) {
				$field_value = $field['default'];
			}
			
			?>
			<div class="cf-metafield <?php echo $field['class']; ?>">
				<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
				<select id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>">
					<?php if( $field['options'] ) {
						foreach( $field['options'] as $key => $value ) { ?>
							<option value="<?php echo $key; ?>" <?php selected( $field_value, $key ); ?>><?php echo $value; ?></option>
						<?php }
					} ?>
				</select>
			</div>
			<?php
		}
		
		/*
		 * Save
		 * @since 1.0.0
		 */
		public function save_metabox_data( $post_id ) {
			
			// Check the nonce is set
			if( ! isset( $_POST['cf_metabox_nonce'] ) ) {
				return;
			}
			
			// Verify the nonce
			if( ! wp_verify_nonce( $_POST['cf_metabox_nonce'], 'save_metabox_data' ) ) {
				return;
			}
			
			// If this is an autosave, our form has not been submitted, so we don't want to do anything.
			if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return;
			}
			
			// Check the user's permissions.
			if( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}
			
			// Save all our metaboxess
			$metaboxes = $this -> metaboxes;
			foreach( $metaboxes as $metabox ) {
				if( $metabox['fields'] ) {
					foreach( $metabox['fields'] as $field ) {
						
						if( $field['type'] != 'divider' ) {
							
							if( isset( $_POST[$field['name']] ) ) {
								if( $field['type'] == 'wysiwyg' ) {
									$data = $_POST[$field['name']];
								} else {
									$data = sanitize_text_field( $_POST[$field['name']] );
								}
								update_post_meta( $post_id, $field['ID'], $data );
							} else {
								delete_post_meta( $post_id, $field['ID'] );
							}
						}
					}
				}
			}
			
		}
	
		
		
		/*
		 * Enqueue styles and scripts
		 * @since 1.0.0
		 */
		public function enqueue_scripts() {
			wp_enqueue_style( 'cf-admin-style', CF_PLUGIN_URL . 'assets/css/admin-style.css' );
			wp_enqueue_media();

			wp_register_script( 'cf-admin', CF_PLUGIN_URL . 'assets/js/admin.js', array( 'jquery' ), time() );
			wp_localize_script( 'cf-admin', 'meta_image',
				array(
					'title' => __( 'Add Image', 'crystal-fairy-plugin' ),
					'button' => __( 'Select', 'crystal-fairy-plugin' ),
				)
			);
			wp_enqueue_script( 'cf-admin' );
			
		//	wp_enqueue_script( 'wp-color-picker' );
		//	wp_enqueue_style( 'wp-color-picker' );
		}

	}
	
}