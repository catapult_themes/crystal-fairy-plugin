<?php
/*
Plugin Name: Crystal Fairy
Plugin URI: https://catapult.website/
Description: Plugin for Crystal Fairy
Version: 1.0.0
Author: Catapult
Author URI: https://catapult.website/
Text Domain: crystal-fairy-plugin
Domain Path: /languages
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function cf_plugin_load_plugin_textdomain() {
    load_plugin_textdomain( 'crystal-fairy-plugin', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'cf_plugin_load_plugin_textdomain' );

/**
 * Define constants
 **/
if ( ! defined( 'CF_PLUGIN_URL' ) ) {
	define( 'CF_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}
if ( ! defined( 'CF_PLUGIN_DIR' ) ) {
	define( 'CF_PLUGIN_DIR', dirname( __FILE__ ) );
}

if ( is_admin() ) {
	require_once dirname( __FILE__ ) . '/admin/class-cf-metaboxes.php';
	require_once dirname( __FILE__ ) . '/admin/metaboxes.php';
	// Our metaboxes
	$metaboxes = cf_plugin_metaboxes();
	$CF_Metaboxes = new CF_Metaboxes( $metaboxes );
	$CF_Metaboxes -> init();
}

require_once dirname( __FILE__ ) . '/inc/functions-testimonials.php';