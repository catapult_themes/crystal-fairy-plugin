<?php
/**
 * Testimonials functions
 */

// We're not using this here because we've already enqueued Isotope
function cf_testimonials_enqueue_scripts() {
	wp_register_script( 'jquery-masonry', '', array( 'jquery' ) );
}
// add_action( 'wp_enqueue_scripts', 'cf_testimonials_enqueue_scripts' );
		
/**
 * Create the post type
 */
if( ! function_exists( 'cf_register_testimonials' ) ) {
	function cf_register_testimonials() {
		$labels = array(
			'name'               => _x( 'Testimonials', 'post type general name', 'crystal-fairy-plugin' ),
			'singular_name'      => _x( 'Testimonial', 'post type singular name', 'crystal-fairy-plugin' ),
			'menu_name'          => _x( 'Testimonials', 'admin menu', 'crystal-fairy-plugin' ),
			'name_admin_bar'     => _x( 'Testimonial', 'add new on admin bar', 'crystal-fairy-plugin' ),
			'add_new'            => _x( 'Add New', 'Testimonial', 'crystal-fairy-plugin' ),
			'add_new_item'       => __( 'Add New Testimonial', 'crystal-fairy-plugin' ),
			'new_item'           => __( 'New Testimonial', 'crystal-fairy-plugin' ),
			'edit_item'          => __( 'Edit Testimonial', 'crystal-fairy-plugin' ),
			'view_item'          => __( 'View Testimonial', 'crystal-fairy-plugin' ),
			'all_items'          => __( 'All Testimonials', 'crystal-fairy-plugin' ),
			'search_items'       => __( 'Search Testimonials', 'crystal-fairy-plugin' ),
			'parent_item_colon'  => __( 'Parent Testimonial:', 'crystal-fairy-plugin' ),
			'not_found'          => __( 'No testimonials found.', 'crystal-fairy-plugin' ),
			'not_found_in_trash' => __( 'No testimonials found in Trash.', 'crystal-fairy-plugin' )
		);
		$args = array(
			'labels'            => $labels,
			'description'       => __( 'Description.', 'crystal-fairy-plugin' ),
			'public'            => false,
			'publicly_queryable'=> false,
			'show_ui'           => true,
			'show_in_menu'      => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'testimonials' ),
			'capability_type'   => 'post',
			'menu_icon'			=> 'dashicons-smiley',
			'has_archive'       => false,
			'hierarchical'      => true,
			'menu_position'     => null,
			'supports'          => array( 'title', 'editor', 'thumbnail' )
		);
		register_post_type( 'testimonials', $args );
	}
}
add_action( 'init', 'cf_register_testimonials' );

/**
 * Print testimonials
 */
if( ! function_exists( 'cf_get_testimonials' ) ) {
	function cf_get_testimonials( $number=-1 ) {
		global $post;
		$args = array(
			'post_type'			=> 'testimonials',
			'posts_per_page'	=> $number
		);
		$return = '';
		$testimonials = new WP_Query( $args );
		if( $testimonials->have_posts() ) {
			while( $testimonials->have_posts() ):
				$testimonials->the_post();
				$return .= '<div class="x-small-12 medium-6 large-4 testimonial-item">';
					$return .= '<div class="testimonial">';
						$return .= cf_get_testimonial( $post->ID );
					$return .= '</div>';
				$return .= '</div>';
			endwhile;
		}
		return $return;
	}
}

/**
 * Print testimonials
 */
if( ! function_exists( 'cf_testimonials_shortcode' ) ) {
	function cf_testimonials_shortcode( $atts ) {
		$atts = shortcode_atts( array(
			'number'	=> -1,
			'layout'	=> 'masonry'
		), $atts );
		$return = '<div id="testimonials">';
		$return .= cf_get_testimonials( $atts['number'] );
		$return .= '</div>';
		if( empty( $layout ) || $layout == 'masonry' ) {
			$return .= "<script>
			jQuery(document).ready(function($){
				container = $('#testimonials');
				container.isotope({
					itemSelector: '.testimonial-item',
					layoutMode: 'masonry',
					percentPosition: true
				});
				container.imagesLoaded().progress(function(){
					container.isotope('layout');
				});
			});
			</script>";
		}
		return $return;
	}
}
add_shortcode( 'cf_testimonials', 'cf_testimonials_shortcode' );

/**
 * Print featured testimonials
 */
if( ! function_exists( 'cf_get_featured_testimonials' ) ) {
	function cf_get_featured_testimonials() {
		global $post;
		$number = get_theme_mod( 'home-testimonials', 3 );
		$args = array(
			'post_type'			=> 'testimonials',
			'posts_per_page'	=> absint( $number ),
			'meta_key'			=> 'cf_featured_testimonial',
			'meta_value'		=> true
		);
		$return = '';
		$testimonials = new WP_Query( $args );
		if( $testimonials->have_posts() ) {
			while( $testimonials->have_posts() ):
				$testimonials->the_post();
				$return .= '<div class="x-small-12 medium-6 large-4">';
					$return .= '<div class="homepage-testimonial testimonial">';
						$return .= cf_get_testimonial( $post->ID );
					$return .= '</div>';
				$return .= '</div>';
			endwhile;
		}
		return $return;
	}
}

/**
 * Get the testimonial content
 */
if( ! function_exists( 'cf_get_testimonial' ) ) {
	function cf_get_testimonial( $post_id ) {
		$return = '';
		$return .= wpautop( get_the_content() );
		$thumbnail = '';
		if( has_post_thumbnail() ) {
			$thumbnail = get_the_post_thumbnail( $post_id, 'thumbnail' );
		}
		$date = get_post_meta( $post_id, 'cf_testimonial_date', true );
		if( ! empty( $date ) ) {
			$date = sprintf( '<span class="testimonial-date">%s</span>', esc_html( $date ) );
		}
		$return .= sprintf( 
			'<div class="testimonial-meta-wrapper"><span class="testimonial-avatar">%s</span><div class="testimonial-meta"><h3>%s</h3>%s</div></div>',
			$thumbnail,
			get_the_title(),
			$date
		);
		return $return;
	}
}