=== Crystal Fairy Plugin ===
Contributors: Catapult_Themes
Tags: 
Requires at least: 4.4
Tested up to: 4.7.2
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Bits for Crystal Fairy

== Description ==



== Installation ==

= Automatic installation =

Log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type "Feature Blocks" and click Search Plugins. Install the plugin by simply clicking "Install Now".


== Frequently Asked Questions ==

= Where can I find the documentation? =

All the documentation is [here](http://catapultthemes.com/).


== Screenshots ==

1. Example


== Changelog == 

= 1.0.0 - 10/11/2016 =

* Initial commit


== Upgrade Notice ==

Nothing here
