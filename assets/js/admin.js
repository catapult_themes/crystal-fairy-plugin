jQuery(document).ready(function($){
	
    // Instantiates the variable that holds the media library frame.
    var meta_image_frame;
 
    // Runs when the image button is clicked.
    $('body').on('click','.cf-work-media-upload',function(e){
 
        // Prevents the default action from occurring.
        e.preventDefault();
 
		// The metabox id
		metabox_id = $(this).attr('data-metabox');
		console.log(metabox_id);
		
        // If the frame already exists, re-open it.
        if ( meta_image_frame ) {
            meta_image_frame.open();
            return;
        }
 
        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: meta_image.title,
            button: { text:  meta_image.button },
            library: { type: 'image' }
        });
 
        // Runs when an image is selected.
        meta_image_frame.on('select', function(){
            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = meta_image_frame.state().get('selection').first().toJSON();
 
            // Sends the attachment URL to our custom image input field.
            $('#item-'+metabox_id+' input.cf-image-id').val(media_attachment.id);
			if( media_attachment.sizes.medium!=undefined ){
				$('#item-'+metabox_id+' .gallery-thumb').html('<img src="'+media_attachment.sizes.medium.url+'">');
			} else {
				$('#item-'+metabox_id+' .gallery-thumb').html('<img src="'+media_attachment.sizes.full.url+'">');
			}
            
        });
 
        // Opens the media library frame.
        meta_image_frame.open();
    });
	// Remove the image
	$('.cf-work-media-remove').click(function(e){
		e.preventDefault();
		// The metabox id
		var metabox_id = $(this).attr('data-metabox');
		$('#'+metabox_id).val('');
		$('#item-'+metabox_id+' .gallery-thumb').html('');
		
	});
});